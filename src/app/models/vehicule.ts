export class Vehicule {
  id: number;
  marque: string;
  modele: string;
  energie: string;
  constructor(id: number = null , marque: string = null, modele: string = null, energie: string = null) {
    this.id = id;
    this.marque = marque;
    this.modele = modele;
    this.energie = energie;
  }
}
