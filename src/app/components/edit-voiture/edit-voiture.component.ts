import { Component, OnInit } from '@angular/core';
import {Vehicule} from '../../models/vehicule';
import {VehiculeService} from '../../services/vehicule.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-edit-voiture',
  templateUrl: './edit-voiture.component.html',
  styleUrls: ['./edit-voiture.component.css']
})
export class EditVoitureComponent implements OnInit {
  voitureForm: Vehicule;
  energieDisponible: string[];
  marqueDisponible: string[];
  energy: string;
  constructor(private vehiculeService: VehiculeService,
              private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.voitureForm = this.vehiculeService.getById(+this.activatedRoute.snapshot.paramMap.get('id'));
    this.marqueDisponible = this.vehiculeService.marqueDispo;
    this.energieDisponible = this.vehiculeService.energieDispo;
    this.energy = this.activatedRoute.snapshot.paramMap.get('energie');
  }

  submitVehicule() {
    this.vehiculeService.update(this.voitureForm);
    this.router.navigate([ this.energy]);
  }

}
