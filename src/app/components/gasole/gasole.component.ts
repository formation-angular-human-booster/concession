import { Component, OnInit } from '@angular/core';
import {Vehicule} from '../../models/vehicule';
import {VehiculeService} from '../../services/vehicule.service';

@Component({
  selector: 'app-gasole',
  templateUrl: './gasole.component.html',
  styleUrls: ['./gasole.component.css']
})
export class GasoleComponent implements OnInit {
  vehicules: Vehicule[];
  constructor(private vehiculeService: VehiculeService) { }

  ngOnInit() {
    this.vehicules = this.vehiculeService.getByEnergie('gasole');
  }

}
