import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GasoleComponent } from './gasole.component';

describe('GasoleComponent', () => {
  let component: GasoleComponent;
  let fixture: ComponentFixture<GasoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GasoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GasoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
