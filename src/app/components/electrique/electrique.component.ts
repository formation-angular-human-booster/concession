import { Component, OnInit } from '@angular/core';
import {Vehicule} from '../../models/vehicule';
import {VehiculeService} from '../../services/vehicule.service';

@Component({
  selector: 'app-electrique',
  templateUrl: './electrique.component.html',
  styleUrls: ['./electrique.component.css']
})
export class ElectriqueComponent implements OnInit {
  vehicules: Vehicule[];
  constructor(private vehiculeService: VehiculeService) { }

  ngOnInit() {
    this.vehicules = this.vehiculeService.getByEnergie('electrique');
  }

  deleteVehicule(vehicule: Vehicule) {
    this.vehicules = this.vehiculeService.remove(vehicule);
  }


}
