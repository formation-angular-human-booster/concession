import { Component, OnInit } from '@angular/core';
import {Vehicule} from '../../models/vehicule';
import {VehiculeService} from '../../services/vehicule.service';

@Component({
  selector: 'app-essence',
  templateUrl: './essence.component.html',
  styleUrls: ['./essence.component.css']
})
export class EssenceComponent implements OnInit {
  vehicules: Vehicule[];
  constructor(private vehiculeService: VehiculeService) { }

  ngOnInit() {
    this.vehicules = this.vehiculeService.getByEnergie('essence');
  }

  deleteVehicule(vehicule: Vehicule) {
    this.vehicules = this.vehiculeService.remove(vehicule);
  }


}
