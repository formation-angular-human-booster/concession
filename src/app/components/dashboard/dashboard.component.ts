import { Component, OnInit } from '@angular/core';
import {Vehicule} from '../../models/vehicule';
import {VehiculeService} from '../../services/vehicule.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  vehicules: Vehicule[];
  constructor(private vehiculeService: VehiculeService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.vehicules = this.vehiculeService.vehicules;
  }

  deleteVehicule(vehicule: Vehicule) {
    this.vehicules = this.vehiculeService.remove(vehicule);
  }

}
