import { Component, OnInit } from '@angular/core';
import {Vehicule} from '../../models/vehicule';
import {VehiculeService} from '../../services/vehicule.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-voiture',
  templateUrl: './add-voiture.component.html',
  styleUrls: ['./add-voiture.component.css']
})
export class AddVoitureComponent implements OnInit {
  voitureForm: Vehicule;
  marqueDisponible: string[];
  energieDisponible: string[];
  energy: string;

  constructor(private vehiculeService: VehiculeService, private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.voitureForm = new Vehicule();
    this.marqueDisponible = this.vehiculeService.marqueDispo;
    this.energieDisponible = this.vehiculeService.energieDispo;
    this.energy = this.activatedRoute.snapshot.paramMap.get('energie');

  }

  submitVehicule() {
    if ( !this.voitureForm.energie ) {
      this.voitureForm.energie = this.energy;
    }
    this.vehiculeService.save(this.voitureForm);
    this.router.navigate(['/', this.energy]);

  }
}
