import { Injectable } from '@angular/core';
import {Vehicule} from '../models/vehicule';

@Injectable({
  providedIn: 'root'
})
export class VehiculeService {
  vehicules: Vehicule[];
  marqueDispo = ['Renault', 'BMW', 'Citroen', 'Tesla'];
  energieDispo = ['essence', 'gasole', 'electrique'];
  constructor() {
    this.vehicules = [
      new Vehicule(1,'Renault', 'Clio', 'essence'),
      new Vehicule(2,'BMW', 'Serie 1', 'gasole'),
      new Vehicule(3,'Renault', 'Zoe', 'electrique')
    ];
  }
  getByEnergie(energie: string) {
    return this.vehicules.filter(vehicule => vehicule.energie === energie);
  }
  save(vehicule: Vehicule) {
    vehicule.id = new Date().getUTCMilliseconds();
    this.vehicules.push(vehicule);
  }
  getById(id: number): Vehicule{
    return this.vehicules.filter(vehicule => vehicule.id === id)[0];
  }
  update(voiture: Vehicule) {
    return this.vehicules.filter(vehicule => vehicule.id === voiture.id)[0] = voiture;
  }
  remove(voiture: Vehicule): Vehicule[] {
    this.vehicules =
      this.vehicules.filter(vehicule => vehicule.id !== voiture.id);
    return this.vehicules;
  }
}
