import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EssenceComponent } from './components/essence/essence.component';
import { GasoleComponent } from './components/gasole/gasole.component';
import { ElectriqueComponent } from './components/electrique/electrique.component';
import { MenuComponent } from './components/menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AddVoitureComponent } from './components/add-voiture/add-voiture.component';
import {FormsModule} from '@angular/forms';
import { EditVoitureComponent } from './components/edit-voiture/edit-voiture.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    EssenceComponent,
    GasoleComponent,
    ElectriqueComponent,
    MenuComponent,
    AddVoitureComponent,
    EditVoitureComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
