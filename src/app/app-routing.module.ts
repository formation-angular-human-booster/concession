import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {EssenceComponent} from './components/essence/essence.component';
import {ElectriqueComponent} from './components/electrique/electrique.component';
import {GasoleComponent} from './components/gasole/gasole.component';
import {AddVoitureComponent} from './components/add-voiture/add-voiture.component';
import {EditVoitureComponent} from './components/edit-voiture/edit-voiture.component';


const routes: Routes = [
  {
    path: '', redirectTo: '/dashboard', pathMatch:  'full'
  },
  {
    path: 'dashboard', component:  DashboardComponent
  },
  {
    path: ':energie/add', component:  AddVoitureComponent
  },
  {
    path: ':energie/edit/:id', component:  EditVoitureComponent
  },
  {
    path: 'essence', component:  EssenceComponent
  },
  {
    path: 'gasole', component:  GasoleComponent
  },
  {
    path: 'electrique', component:  ElectriqueComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
